const jwt = require('jsonwebtoken');
const { syncIndexes } = require('mongoose');

const { Load } = require('../models/Loads');
const { Truck, truckSize } = require('../models/Trucks');

const changeLoadStatusMiddleware = async (req, res, next) => {
  const status = 'POSTED';
  try {
    const load = await Load.findByIdAndUpdate({ _id: req.id }, { $set: { status: status } });
    next();
    await load.save();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const findTruckMiddleware = async (req, res, next) => {
  try {
    const load = await Load.findById(req.id);
    const trucks = await Truck.aggregate([
      {
        '$match': {
          'status': 'IS'
        }
      }
    ])
    
    const filterTrucks = trucks.filter((truck) => {
      const size = truckSize[truck.type];
      return truck.assigned_to !== 'none' && 
      size.payload > load.payload &&
      size.dimensions.width > load.dimensions.width &&
      size.dimensions.length > load.dimensions.length &&
      size.dimensions.height > load.dimensions.height
    })

    if (!filterTrucks.length) {
      load.status = 'NEW';
      load.logs.push({
        message: 'Driver was not found',
        time: new Date().toISOString()
      })
      await load.save();
    } else {
      const driverId = filterTrucks[0].assigned_to;
      load.status = 'ASSIGNED';
      load.state = 'En route to Pick Up';
      load.assigned_to = driverId;
      await load.save();
      req.driverId = driverId
      next()
    }
  
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const assignTruckMiddleware = async (req, res, next) => {
  try {
    const userId =  req.driverId;
    const truck = await Truck.findOne({assigned_to: userId});
    truck.status = "OL";
    next();
    return await truck.save();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const finishArriveMiddleware = async (req, res, next) => {
  try {
    const truck = await Truck.findOne({assigned_to: req.user.userId});
    truck.status = "IS";

    return await truck.save();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const finishLoadStatus = async (req, res, next) => {
  try {
    console.log()
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  changeLoadStatusMiddleware,
  findTruckMiddleware,
  assignTruckMiddleware,
  finishArriveMiddleware,
  finishLoadStatus
};
