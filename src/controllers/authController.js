const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../models/Users');
const { saveUser } = require('../services/authService');

const registerUser = async (req, res, next) => {

  try {
    const { email, password, role } = req.body;
    const createdDate = new Date().toISOString();
    await userJoiSchema.validateAsync({email, password, role, createdDate});

    const user = await saveUser({email, password, role, createdDate})
    return await res.status(200).send({ "message": "Profile created successfully" });
  } catch(e) {
    console.log(e)
    return res.status(400).json({ message: 'User Error' })
  }
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, userId: user._id };
    const jwtToken = jwt.sign(payload, 'jwt_token');
    return res.status(200).json({ jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const forgotPassword = async (req, res, next) => {
  try {
    const { email } = req.body;
    return await res.status(200).send({ "message": "New password sent to your email address" });
  } catch(e) {
    return res.status(400).json({ message: e.message })
  }
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword
};
