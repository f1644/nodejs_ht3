const { Load, loadJoiSchema } = require('../models/Loads');
const { User } = require('../models/Users');
const { saveLoad } = require('../services/loadsService');

const createLoad = async (req, res, next) => {
  try {
    const loadId = req.params.id; 
    req.id = loadId;
    const user = await User.findById({_id: req.user.userId});
    if (user.role === "SHIPPER") {
      const { name, payload, pickup_address, delivery_address, dimensions} = req.body;
      const created_date = new Date().toISOString();
      const created_by = req.user.userId; 
      await loadJoiSchema.validateAsync({
        created_by,
        assigned_to: 'none',
        status: 'NEW',
        state: 'En route to Pick Up',
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions: {...dimensions},
        logs: [],
        created_date
      })
      const load = await saveLoad({created_by, name, payload, pickup_address, delivery_address, dimensions, created_date}); 
      return await  res.status(200).send({ "message": "Load created successfully" });
    }
    return res.status(400).json({ message: "Load was not creat" });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const checkLoad = async (req, res, next) => {
  try {
    const loadId = req.params.id; 
    req.id = loadId;
    const user = await User.findById({_id: req.user.userId});
    if (user.role === "SHIPPER") {
      next();
    } else {
      return await  res.status(400).send({ "message": "Load was not created" });
    }
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const postLoad = async (req, res, next) => {
  try {
    return await  res.status(200).send({
      "message": "Load posted successfully",
      "driver_found": true
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const getUserLoads = (req, res, next) => {
  try {
    return Load.find({ userId: req.user.userId }, '-__v').then((result) => {
      res.json({loads: result});
    });
  } catch (e) {
    res.status(400).send({ "message": "Get loads error" });
  } 
}

const getLoadById = (req, res, next) => {
  try {
    return Load.findById(req.params.id, '-__v')
      .then((response) => {
        res.json({ load: response });
      });
  } catch (e) {
    res.status(400).send({ "message": "Get load error" });
  } 
}

const updateUserLoadById = (req, res, next) => {
  try {
    const newLoad = req.body;
    return Load.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { ...newLoad }})
    .then(() => {
      res.status(200).send({ "message": "Load details changed successfully" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Update error" });
  }
};

const deleteUserLoadById = (req, res, next) => {
  try {
    Load.findByIdAndDelete({ _id: req.params.id, userId: req.user.userId })
    .then(() => {
      res.status(200).send({ "message": "Load deleted successfully" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Delete error" });
  }
} 

const getLoadShippingInfoById = (req, res, next) => {
  try {
    return Load.findById(req.params.id, '-__v')
      .then((response) => {
        res.json({ load: response });
      });
  } catch (e) {
    res.status(400).send({ "message": "Get load error" });
  } 
}

const changeLoadState = async (req, res, next) => {
  try {
    const userId = req.user.userId;
    const user = await User.findById({_id: userId});
    
    if (user.role === "DRIVER") {
      const load = await Load.findOneAndUpdate({assigned_to: userId}, { $set: { state:  'Arrived to delivery', status: 'SHIPPED'} });
      next();
      await res.status(200).send({ "message": "Load state changed to 'Arrived to delivery'" });
    } else {
      await res.status(400).send({ "message": "Load was not change" });
    }
    
  } catch (e) {
    res.status(400).send({ "message": e.message });
  }
};

const getActiveLoad = (req, res, next) => {
  try {
    return Load.findOne({}, '-__v').then((result) => { 
      res.json({'load': result});
    });
  } catch (e) {
    res.status(400).send({ "message": "Get loads error" });
  } 
}


module.exports = {
  createLoad,
  checkLoad,
  postLoad,
  getUserLoads,
  getLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  getLoadShippingInfoById,
  changeLoadState,
  getActiveLoad
};