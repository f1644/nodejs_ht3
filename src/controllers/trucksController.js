const { Truck, truckJoiSchema } = require('../models/Trucks');
const { saveTruck } = require('../services/trucksService');

const getUserTrucks = (req, res, next) => {
  try {
    return Truck.find({ userId: req.user.userId }, '-__v').then((result) => {
      res.json({trucks: result});
    });
  } catch (e) {
    res.status(400).send({ "message": "Get trucks error" });
  } 
} 

const createTruck = async (req, res, next) => {
  try {
    const { type } = req.body;
    const createdDate = new Date().toISOString();
    const created_by = req.user.userId; 
    await truckJoiSchema.validateAsync({
      created_by,
      assigned_to: 'none',
      type,
      status: 'IS',
      createdDate
    })
    const truck = await saveTruck({created_by, type, createdDate})
    return await  res.status(200).send({ "message": "Truck created successfully" });
  } catch (e) {
    res.status(400).send({ "message": "Creating error" });
  }
}

const getTruckById = (req, res, next) => {
  try {
    return Truck.findById(req.params.id, '-__v')
      .then((response) => {
        res.json({ truck: response });
      });
  } catch (e) {
    res.status(400).send({ "message": "Get truck error" });
  } 
}

const updateUserTruckById = (req, res, next) => {
  try {
    const { type } = req.body;
    return Truck.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { type } })
    .then(() => {
      res.status(200).send({ "message": "Truck details changed successfully" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Update error" });
  }
};

const deleteUserTruckById = (req, res, next) => {
  try {
    Truck.findByIdAndDelete({ _id: req.params.id, userId: req.user.userId })
    .then(() => {
      res.status(200).send({ "message": "Truck deleted successfully" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Delete error" });
  }
} 

const assignUserTruckById = (req, res, next) => {
  try {
    return Truck.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { assigned_to:  req.user.userId} })
    .then(() => {
      res.status(200).send({ "message": "Truck assigned successfully" });
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getUserTrucks,
  createTruck,
  getTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById
};
