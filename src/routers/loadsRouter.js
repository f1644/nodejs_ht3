const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddleware');
const { changeLoadStatusMiddleware, assignTruckMiddleware, findTruckMiddleware, finishArriveMiddleware } = require('../middleware/loadsMiddleware');
const {
  createLoad,
  postLoad,
  checkLoad,
  getUserLoads,
  getLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  getLoadShippingInfoById,
  changeLoadState,
  getActiveLoad
} = require ('../controllers/loadsController');

router.get('/', authMiddleware, getUserLoads);
router.post('/', authMiddleware, createLoad);
router.get('/active', authMiddleware, getActiveLoad);
router.patch('/active/state',
  authMiddleware,
  changeLoadState,
  finishArriveMiddleware,
);
router.post('/:id/post',
  authMiddleware,
  checkLoad,
  changeLoadStatusMiddleware,
  findTruckMiddleware,
  assignTruckMiddleware,
  postLoad,
);
router.get('/:id', authMiddleware, getLoadById);
router.put('/:id', authMiddleware, updateUserLoadById);
router.delete('/:id', authMiddleware, deleteUserLoadById);
router.get('/:id/shipping_info', authMiddleware, getLoadShippingInfoById);


module.exports = {
  loadsRouter: router,
};