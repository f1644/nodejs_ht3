const express = require('express');

const router = express.Router();
const { getUserInfo, changeUserPassword, deleteUser } = require('../controllers/usersController');
const { authMiddleware } = require('../middleware/authMiddleware');


router.get('/me', authMiddleware, getUserInfo);

router.patch('/me', authMiddleware, changeUserPassword);

router.delete('/me', authMiddleware, deleteUser);

module.exports = {
  usersRouter: router,
};
