const express = require('express');
const router = express.Router();

const { authMiddleware } = require('../middleware/authMiddleware');
const { getUserTrucks, createTruck, getTruckById, assignUserTruckById, updateUserTruckById, deleteUserTruckById  } = require ('../controllers/trucksController');

router.get('/', authMiddleware, getUserTrucks);
router.post('/', authMiddleware, createTruck);
router.get('/:id', authMiddleware, getTruckById);
router.put('/:id', authMiddleware, updateUserTruckById);
router.delete('/:id', authMiddleware, deleteUserTruckById);
router.post('/:id/assign', authMiddleware, assignUserTruckById);

module.exports = {
  trucksRouter: router,
};