const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://Kate:Some-password@cluster0.7qdjksr.mongodb.net/logisticApp?retryWrites=true&w=majority');

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);


const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: 'Server error' });
}
