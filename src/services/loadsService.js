const { Load } = require('../models/Loads');

const saveLoad = async ({ created_by, name, payload, pickup_address, delivery_address, dimensions, created_date }) => {
  const load = new Load({
    created_by,
    assigned_to: 'none',
    status: 'NEW',
    state: 'En route to Pick Up',
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {...dimensions},
    logs: [],
    created_date
  });
  return await load.save();
}

module.exports = {
  saveLoad
}