const { Truck } = require('../models/Trucks');

const saveTruck = async ({ created_by, type, createdDate }) => {
  const truck = new Truck({
    created_by,
    assigned_to: 'none',
    type,
    status: 'IS',
    createdDate
  });
  return await truck.save();
}

module.exports = {
  saveTruck
}