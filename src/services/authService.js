const { User } = require('../models/Users');
const bcryptjs = require('bcryptjs');

const saveUser = async ({ email, password, role, createdDate }) => {

    const user = new User({
      email,
      password: await bcryptjs.hash(password, 10),
      role,
      createdDate
    });
  
    return await user.save();
  
}

module.exports = {
  saveUser
}