const mongoose = require('mongoose');
const Joi = require('joi');

const loadJoiSchema = Joi.object({
  created_by: Joi.string()
    .required(),

  assigned_to: Joi.string()
    .required(),

  status: Joi.string()
    .pattern(new RegExp('^(NEW|POSTED|ASSIGNED|SHIPPED)$'))
    .required(),
  
  state: Joi.string()
    .pattern(new RegExp('^(En route to Pick Up|Arrived to Pick Up|En route to delivery|Arrived to delivery)$'))
    .required(),

  name: Joi.string()
    .required(),

  payload: Joi.number()
    .required(),

  pickup_address: Joi.string()
    .required(),

  delivery_address: Joi.string()
    .required(),

  dimensions: Joi.object({
    width: Joi.number()
      .required(),

    length: Joi.number()
      .required(),

    height: Joi.number()
      .required(),
  }),

  logs: Joi.array()
    .required(),
    
  created_date: Joi.string()
    .required(),
});

const loadSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {
      width: Number,
      length: Number,
      height: Number,
    },
    required: true,
  },
  logs: [{
    type: Array,
    required: true,
  }],
  created_date: {
    type: String,
    required: true,
  }
});

const Load = mongoose.model('load', loadSchema);


module.exports = {
  Load,
  loadJoiSchema
};
