const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiSchema = Joi.object({
  created_by: Joi.string()
    .required(),

  assigned_to: Joi.string()
    .required(),

  type: Joi.string()
    .pattern(new RegExp('^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$'))
    .required(),

  status: Joi.string()
    .pattern(new RegExp('^(OL|IS)$'))
    .required(),
  
  createdDate: Joi.string()
    .required(),
});

const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  }
});

const Truck = mongoose.model('truck', truckSchema);

const truckSize = {
  'SPRINTER': {
    payload: 1700,
    dimensions: {
      width: 300,
      length: 250,
      height: 170
    }
  },
  'SMALL STRAIGHT': {
    payload: 2500,
    dimensions: {
      width: 500,
      length: 250,
      height: 170
    }
  },
  'LARGE STRAIGHT': {
    payload: 4000,
    dimensions: {
      width: 700,
      length: 250,
      height: 170
    }
  }
}

module.exports = {
  Truck,
  truckJoiSchema,
  truckSize
};
